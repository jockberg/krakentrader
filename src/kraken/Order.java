package kraken;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Order implements Comparable<Order> {
	
	
	private boolean open;
	private String id;
	private double eur,btc,fee,rate;
	private int counttarget=0;
	private boolean selling=false;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public double getEur() {
		return eur;
	}
	public void setEur(double eur) {
		this.eur = eur;
	}
	public double getBtc() {
		return btc;
	}
	public void setBtc(double btc) {
		this.btc = btc;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	
	public String toString(){
		DecimalFormat df = new DecimalFormat("#.####");
		df.setRoundingMode(RoundingMode.HALF_DOWN);
		
		return "ID:"+id+" ,eur:"+df.format(eur)+" ,btc:"+df.format(btc);
	}
	
	public double calcRate(){
		return eur/btc;
	}
	
	public double getTradeRate(){
		return rate;
	}
	
	public void setRate(double rate){
		this.rate=rate;
	}
	
	   @Override
	    public int compareTo(Order o) {
	     
		   if(Math.abs(calcRate())>Math.abs(o.calcRate())){
			   return 1;
		   }else if ((Math.abs(calcRate())<Math.abs(o.calcRate()))){
			   return -1;
		   }else{
			   return 0 ;
		   }
	       
	    }
	public int getCounttarget() {
		return counttarget;
	}
	public void setCounttarget(int counttarget) {
		this.counttarget = counttarget;
	}
	public boolean isSelling() {
		return selling;
	}
	public void setSelling(boolean selling) {
		this.selling = selling;
	}
	   
	
	
}
