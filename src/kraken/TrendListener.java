package kraken;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrendListener {

	double current_trend=0;

	int index;
	double[] data ;
	boolean full=false;
	//List<Double> dataPoints = new ArrayList<>();
	public TrendListener(){
		this.data= new double[20];
		this.index = 0;
		this.current_trend=0;
	}
	
	public String addDataPoint(double val){
		
		data[index % 20]=val; 
		index++;
		if(index==20){
			index=0;
			full=true;
		}
		System.out.println("mod:"+(index%20)+", index:"+index);
		return Arrays.toString(data);
		
	}
	
	public double getTrend(){
		if(full){
			double tot=0;
			double score;
			for(double dat :data){
				tot=tot+dat;
			}
				
			double avg=tot/(double)20;
			if(index==0){
			 score=avg-data[19 % 20];
			}else{
			 score=avg-data[index-1 % 20];
			}
			
			System.out.println("average trend:"+avg+" ,trendscore:"+score);
			
			return score;
		}
		return 0;
	
	}
	
}
