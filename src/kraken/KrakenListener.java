package kraken;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface KrakenListener {

	void sell(Order sellOrder) throws InvalidKeyException, NoSuchAlgorithmException, IOException;

	void buy(Order buyOrder)throws InvalidKeyException, NoSuchAlgorithmException, IOException;

	
	
	
}
