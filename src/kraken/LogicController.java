package kraken;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class LogicController {

	private Orders orders;
	private double myEur=0.0, myBtc=0.0, sellLimit=0.01, tradingEurBalance=0;
	BuyController buyer;
	SellController seller;
	KrakenListener kraken;
	boolean activeOrder=false;
	double latest=0.0;
	private double[] ticker;
	int ongoingOrderCounter = 0; 
	List<Order> sellOrders;
	private Screen screen;

	public LogicController(KrakenListener listener){
		this.kraken=listener;
		orders = new Orders();
		orders.loadSold();
		buyer = new BuyController();
		seller = new SellController();
		screen = new Screen();
	}

	public boolean loadOrders(Orders newOrders) {
		if(newOrders!=null){
			for(String str : orders.sold){
				newOrders.removeOrder(str);
			}
			newOrders.sold=orders.sold;
			this.orders=newOrders;
			return true;
		}
		return false;
	}

	public boolean loadBalance(double[] balance) {
		if(balance!=null){
			myEur=balance[0];
			setMyBtc(balance[1]);
			return true;
		}
		return false;
	}

	public boolean loadTicker(double[] ticker) {
		if(ticker!=null){
			this.setTicker(ticker);
			if(Math.abs(ticker[0]-latest)<200 || latest==0.0){
				latest=ticker[0];
				this.setTicker(ticker);
				return true;
			}
		}
		return false;
	}

	public Orders getOrders(){
		return this.orders;
	}

	public void updateOnTrade(){
		buyer.setMinOrderValue(orders);
	}

	public void makeTrades() throws InvalidKeyException, NoSuchAlgorithmException, IOException {

		if(ongoingOrderCounter>=5){
			activeOrder=false;
			ongoingOrderCounter=0;
		}

		if(!activeOrder){
			Order buyOrder = buyer.buyNow(orders, latest);
			
			if(buyOrder==null){
				sellOrders = seller.sellNow(orders, sellLimit, latest);
				if(!orders.orders.isEmpty() && sellOrders!=null &&  !sellOrders.isEmpty()){
					if(sellOrders.size()==1){
						if(sellOrders.get(0).getBtc()<=myBtc){
						kraken.sell(sellOrders.get(0));
						}else{
							orders.soldOrder(sellOrders);
						}
					}else{
						Order multiple =seller.createMultipleSellOrder(sellOrders,latest);
						if(multiple.getBtc()<=myBtc){
						kraken.sell(multiple);
						}else{
							Order first = sellOrders.get(0);
							sellOrders.clear();
							sellOrders.add(first);
							orders.soldOrder(sellOrders);
						}
					}
					activeOrder=true;
					tradingEurBalance=myEur;
					seller.selling=true;
				}
			}else{
				if((buyOrder.getBtc() * latest)<=myEur){
				kraken.buy(buyOrder);
				activeOrder=true;
				tradingEurBalance=myEur;
				buyer.buying=true;
				}else{
				System.out.println("no money");	
				}
			}	
		}else{
			if(myEur!=tradingEurBalance){
				if(seller.selling || buyer.buying){
					if(seller.selling){
						orders.soldOrder(sellOrders);
						seller.selling=false;
					}
					activeOrder = false;
					sellOrders=null;
					buyer.buying=false;
				}
			}
		}
	}

	public List<String> getSold() {
		return orders.sold;
	}

	public double[] getTicker() {
		return ticker;
	}

	public void setTicker(double[] ticker) {
		this.ticker = ticker;
	}

	public double getMyBtc() {
		return myBtc;
	}

	public void setMyBtc(double myBtc) {
		this.myBtc = myBtc;
	}
	public double getMyEur(){
		return this.myEur;
	}
	
	public  void printInfo() {
		clearScreen();
		double[] ticker = getTicker();
		screen.printline("last:"+ticker[0]+", avg:"+ticker[1]+", low:"+ticker[2]+", high:"+ticker[3], screen.ANSI_RESET);
		screen.printTablerow("%-20s%20s%20s%20s%20s", Screen.ANSI_RESET,"order Id          ","Euros","bitcoins","gain in euro","gain i%");
		
		double totalInvest=0.0, totbtc=0.0, totgaineur=0.0;
		for(Order o : getOrders().orders){
			
			double eur= Math.abs(o.getEur());
			double stat =(((double)(o.getBtc()*latest))-eur);
			double percent=stat/eur;
			
			totalInvest+=o.getEur();
			totbtc+=o.getBtc();
			totgaineur+=stat;
			if(o.isSelling()){
			screen.printTablerow("%-20s%20s%20s%20s%20s", Screen.ANSI_GREEN, screen.maskOrder(o.getId()), o.getEur(), o.getBtc(), stat, (percent*100.0));
			}else{
			screen.printTablerow("%-20s%20s%20s%20s%20s", Screen.ANSI_YELLOW, screen.maskOrder(o.getId()), o.getEur(), o.getBtc(), stat, (percent*100.0));
			}
		}
		
		double toteur= Math.abs(totalInvest);
		double totstat =(((double)(totbtc*latest))-toteur);
		double totpercent=totstat/toteur;
		double insatt= 3230.0;
		double current = getMyBtc() * latest;
		double totaleuros= (current+getMyEur()) - insatt;

		screen.printTablerow("%-20s%20s%20s%20s%20s", Screen.ANSI_CYAN, "Total:", totalInvest, totbtc, totgaineur, totpercent*100);
		
		screen.printline("Total euros earned: "+screen.df.format(totaleuros)+ "min order:"+buyer.minValue, Screen.ANSI_RESET);
	}

	public void printError(String msg){
		screen.clearScreen(); 
		screen.printline(msg, screen.ANSI_RED);
	}

	public void clearScreen() {
		screen.clearScreen();
		
	}
	
}
