package kraken;

public class BuyController {

	double minValue=999999, latest=0, buyInlimit=75;
	boolean buying = false;


	public Order buyNow(Orders orders, double latest){
		Order order = null;
		this.latest=latest;
		setMinOrderValue(orders);
		if(isFirstOrder(orders) || getDiffValue()>buyInlimit){
			order = new Order();
			order.setBtc(0.06);
			order.setRate(latest);
		}
		return order;
	}

	public boolean isFirstOrder(Orders orders){
		if(orders!=null && orders.orders!=null && orders.orders.isEmpty()){
			return true;
		}
		return false;
	}

	public void setMinOrderValue(Orders orders){
		 minValue=999999;
		for(Order o:orders.orders){
			minValue=Math.min(minValue,Math.abs(o.calcRate()));
		}
	}

	public double getDiffValue(){
		return minValue-latest;
	}
	
}





