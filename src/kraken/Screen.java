package kraken;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

public class Screen {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public DecimalFormat df;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	String screen="";

	
	public Screen(){
		df =new DecimalFormat("#.####");
		df.setRoundingMode(RoundingMode.HALF_DOWN);
	}
	
	public void scrrenShot() throws IOException, AWTException{
		Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
		BufferedImage capture = new Robot().createScreenCapture(screenRect);
		ImageIO.write(capture, "jpg", new File("/home/itisit/Dropbox/last.jpg"));

	}
	public void printline(String line, String color){
		System.out.println(color+line+ANSI_RESET);
	}
	
	public void printTablerow(String format,String color, String one, double two, double three, double four, double five){
		System.out.println(color);
		System.out.format(format,one ,df.format(two), df.format(three),df.format(four) ,df.format(five));
		System.out.println(ANSI_RESET);
	}
	
	public void printTablerow(String format,String color, String one, String two, String three, String four, String five){
		System.out.println(color);
		System.out.format(format,one ,two, three,four ,five);
		System.out.println(ANSI_RESET);
	}
	
	public void clearScreen(){
		System.out.print("\033[H\033[2J");
		System.out.flush(); 
	}
	
	public void print(){
		System.out.format(screen.toString());
	}
	
	public void printDate(){
		Date date = new Date();
		System.out.println(dateFormat.format(date));
	}
	public String maskOrder(String id){
		String res="xx";

		if(id!=null && !id.isEmpty() && id.contains("-")){
			String mask="XXXXXX";
			String real=id.substring(id.indexOf("-"),id.lastIndexOf("-")+1);
			res=mask+real+mask;
		}
		return res;
	}
	
}
