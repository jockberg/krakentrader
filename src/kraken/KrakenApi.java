package kraken;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class KrakenApi {

	private String key = "api key ";
	private String secret = "api secret";
	private String domain = "https://api.kraken.com";
	private String nonce, signature, path;
	public double latest =0,coins=0.0; 
	public TrendListener trend;
	KrakenListener listener;
	DecimalFormat df = new DecimalFormat("####.#");
	
	public KrakenApi(KrakenListener listener){
		this.trend=new TrendListener();
		this.listener=listener;
	}

	public double[] account_balance() throws InvalidKeyException, NoSuchAlgorithmException {
		nonce = String.valueOf(System.currentTimeMillis());
		path = "/0/private/Balance";

		List<ValuePair> params = new ArrayList<>();
		params.add(new ValuePair("nonce",nonce));
		calculateSignature(getDataString(params));

		String answer = postPrivate(domain + path, params);
		double balance[]=null;
		if(answer!=null && answer.contains("XXBT")){
			try {
				balance= new double[2];
				JSONObject obj = new JSONObject(answer);
				obj = obj.getJSONObject("result");
				balance[0]=Double.parseDouble(obj.getString("ZEUR"));
				balance[1] =Double.parseDouble(obj.getString("XXBT"));
			
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return balance;
	}

	String postPrivate(String address, List<ValuePair> params) {
		String answer = "";
		HttpsURLConnection c = null;
		try {
			URL u = new URL(address); 
			c = (HttpsURLConnection)u.openConnection();
			c.setRequestMethod("POST");
			c.setRequestProperty("API-Key", key);
			c.setRequestProperty("API-Sign", signature);
			c.setConnectTimeout(5000);
			c.setDoOutput(true);

			OutputStream os = c.getOutputStream();
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(os, "UTF-8"));
			writer.write(getQuery(params));
			writer.flush();
			writer.close();
			os.close();

			if(c.getResponseCode()>199 && c.getResponseCode()<207 ){
				BufferedReader br = null;

				br = new BufferedReader(new InputStreamReader(c.getInputStream()));
				String line;
				while ((line = br.readLine()) != null)
					answer += line;
			}
		} catch (Exception x) {
			x.printStackTrace();
		} finally {
			c.disconnect();
		}
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return answer;        
	}

	void calculateSignature(String data) throws NoSuchAlgorithmException, InvalidKeyException {
		signature = "";

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update((nonce + data).getBytes());
		Mac mac = Mac.getInstance("HmacSHA512");
		mac.init(new SecretKeySpec(Base64.getDecoder().decode(secret.getBytes()), "HmacSHA512"));
		mac.update(path.getBytes());
		signature = new String(Base64.getEncoder().encode((mac.doFinal(md.digest()))));
	}

	public double[] getTickerEur() throws UnsupportedEncodingException, JSONException, InvalidKeyException, NoSuchAlgorithmException{
		String res ="";
		path = "/0/public/Ticker";
		List<ValuePair> params = new ArrayList<>();
		params.add(new ValuePair("pair","XXBTZEUR"));

		calculateSignature(getQuery(params));
		String answer = postPrivate(domain + path, params);
		
		double[] ticker = null;

		if(!answer.isEmpty() && answer.contains("XXBTZEUR")){
			ticker=new double[4];
			JSONObject obj = new JSONObject(answer);
			obj = obj.getJSONObject("result");
			obj = obj.getJSONObject("XXBTZEUR");
			JSONArray last  = obj.getJSONArray("c"); // last trade
			JSONArray weight  = obj.getJSONArray("p"); // weighted
			JSONArray low  = obj.getJSONArray("l"); //lowest today 
			JSONArray high  = obj.getJSONArray("h"); // hih today 

			ticker[0] =Double.parseDouble(last.getString(0));
			ticker[1] =Double.parseDouble(weight.getString(0));
			ticker[2] =Double.parseDouble(low.getString(0));
			ticker[3] =Double.parseDouble(high.getString(0));
		}
		return ticker;
	}

	public void clearScreen(){
		System.out.print("\033[H\033[2J");
		System.out.flush(); 
	}

	private String getQuery(List<ValuePair> params) throws UnsupportedEncodingException{
		StringBuilder result = new StringBuilder();
		boolean first = true;

		for (ValuePair pair : params){
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.getData(), "UTF-8"));
		}

		return result.toString();
	}


	
	public void placeSellOrder(Order o) throws InvalidKeyException, NoSuchAlgorithmException, IOException {

		path = "/0/private/AddOrder";
		nonce = String.valueOf(System.currentTimeMillis());
		List<ValuePair> params = new ArrayList<>();
		params.add(new ValuePair("nonce",nonce));
		params.add(new ValuePair("pair","XXBTZEUR"));
		params.add(new ValuePair("type","sell"));
		params.add(new ValuePair("ordertype","limit"));		
		params.add(new ValuePair("price",df.format(o.getTradeRate())));
		params.add(new ValuePair("volume",""+o.getBtc()));
		params.add(new ValuePair("expiretm","+150"));
		//params.add(new ValuePair("userref",""+nonce));

		calculateSignature(getQuery(params));

		String answer = postPrivate(domain + path, params);
		System.out.println(answer);
	}

	public String getDataString(List<ValuePair> params){
		String data="";
		for(ValuePair pair: params){
			data+=pair.getKey()+"="+pair.getData();
		}
		return data;
	}

	public Orders getLedger(List<String> sold) throws JSONException, UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException{
		path = "/0/private/Ledgers";
		nonce = String.valueOf(System.currentTimeMillis());
		List<ValuePair> params = new ArrayList<>();
		params.add(new ValuePair("nonce",nonce));
		params.add(new ValuePair("type","trade"));
		params.add(new ValuePair("start","1511603525"));


		Orders temp = null;
		calculateSignature(getQuery(params));
		String answer = postPrivate(domain + path, params);
		if(!answer.contains("EService")){
			if(!answer.isEmpty() && answer.contains("ledger")){
				temp= new Orders();
				JSONObject obj = new JSONObject(answer);
				//System.out.println(obj);

				obj = obj.getJSONObject("result");
				obj = obj.getJSONObject("ledger");
				JSONArray arr =  obj.names();
				

				if(arr!=null && arr.length()!=0){
					for(int i=0;i<arr.length();i++){
						Order o  = new Order();
						JSONObject ob= obj.getJSONObject(arr.getString(i));
						o.setId(ob.getString("refid"));
						if(temp.exists(o)){
							o=temp.getOrder(o.getId());
						}

						String curr=ob.getString("asset");
						Double amount =  Double.parseDouble(ob.getString("amount"));

						if(curr.equals("XXBT")){
							o.setBtc(amount);
						}else if(curr.equals("ZEUR")){
							o.setEur(amount);
						}
						if(!temp.exists(o) && !sold.contains(o.getId())){
							temp.addOrder(o);
						}
					}

					temp.orders.removeIf(o -> (o.getEur()>0 ||  o.getBtc()<0.01));
					Collections.sort(temp.orders);
				
				}
			}
		}
		return temp;
	}

	public void placeBuyOrder(Order buyOrder) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException{

		path = "/0/private/AddOrder";
		nonce = String.valueOf(System.currentTimeMillis());

		List<ValuePair> params = new ArrayList<>();
		params.add(new ValuePair("nonce",nonce));
		params.add(new ValuePair("pair","XXBTZEUR"));
		params.add(new ValuePair("type","buy"));
		params.add(new ValuePair("ordertype","limit"));
		params.add(new ValuePair("volume",""+buyOrder.getBtc()));
		params.add(new ValuePair("price",df.format(buyOrder.getTradeRate())));
		params.add(new ValuePair("expiretm","+150"));
		//params.add(new ValuePair("userref",""+nonce));

		calculateSignature(getQuery(params));
		String answer = postPrivate(domain + path, params);
		System.out.println(answer);
	}


}
