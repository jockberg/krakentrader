package kraken;

import java.util.ArrayList;
import java.util.List;

public class SellController {

	boolean selling = false;



	public List<Order> sellNow(Orders orders, double sellLimit, double latest) {
		List<Order> sellOrders=new ArrayList<>();
		for(Order o : orders.orders){
			if(o.getEur()<0){
				double eur= Math.abs(o.getEur());
				double stat =(((double)(o.getBtc()*latest))-eur);
				double percent=stat/eur;
				
				if(percent>sellLimit){
					Order sell = new Order();
					sell.setRate(latest);
					sell.setBtc(o.getBtc());
					sell.setId(o.getId());
					sellOrders.add(sell);
					o.setSelling(true);
				}
			}
		}
		return sellOrders;
	}


	public Order createMultipleSellOrder(List<Order> sellOrders, double latest){


		double coins = 0.0;
		for(Order o : sellOrders){
			coins+=o.getBtc();
		}
		Order sellOrder = new Order();
		sellOrder.setBtc(coins);
		sellOrder.setRate(latest);

		return sellOrder;
	}
}


