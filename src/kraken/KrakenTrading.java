package kraken;

import java.awt.AWTException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class KrakenTrading implements KrakenListener{

	private LogicController logic;
	private KrakenApi api;


	public KrakenTrading(){
		this.logic=new LogicController(this);
		this.api=new KrakenApi(this);
	}

	@Override
	public void sell(Order sellOrder) throws InvalidKeyException, NoSuchAlgorithmException, IOException {
		api.placeSellOrder(sellOrder);
	}

	@Override
	public void buy(Order buyOrder) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException {
		api.placeBuyOrder(buyOrder);
	}
	
	public void logic () throws JSONException, IOException, AWTException, InvalidKeyException, NoSuchAlgorithmException{
		
		if(logic.loadOrders(api.getLedger(logic.getSold()))){
			if(logic.loadBalance(api.account_balance())){
				if(logic.loadTicker(api.getTickerEur())){
					logic.makeTrades();
					logic.printInfo();
				}else{logic.printError("Error: ticker not loaded");}
			}else{logic.printError("Error: balance not loaded");;}
		}else{logic.printError("Error: Order info not loaded");}
		if(logic.activeOrder){
			logic.ongoingOrderCounter++;
			System.out.println("ActiveOrder round: "+logic.ongoingOrderCounter+" /5");
		}
	}


	public static void main(String[] args) {

		KrakenTrading t = new KrakenTrading();

		final ScheduledExecutorService scheduler =Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override 
			public void run() {
				try {
					t.logic();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, 2, 40, TimeUnit.SECONDS);}
}


