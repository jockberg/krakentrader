package kraken;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Orders {
	ArrayList<String> sold ;
	List<Order>orders;


	public Orders(){
		this.orders= new ArrayList<>();
		sold = new ArrayList<>();
	}

	public Order getOrder(String id){
		Order or= null;
		for(Order o: orders ){
			if(o.getId().equals(id)){
				return o;
			}	
		}
		return null;
	}

	public boolean exists(Order o){
		for(Order ob : orders){
			if(o.getId().equals(ob.getId())){
				return true;
			}
		}
		return false;
	}

	public void addOrder(Order o){
		orders.add(o);
	}

	public void removeOrder(String id){
		orders.remove(getOrder(id));
	}

	public void loadSold(){
		try(BufferedReader br = new BufferedReader(new FileReader("/home/itisit/sold.txt"))) {
			String line = br.readLine();

			while (line != null) {
				line = br.readLine();
				sold.add(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public void soldOrder(List<Order> soldOrders){
		try {
			FileWriter fw=	new FileWriter("/home/itisit/sold.txt",true);
			BufferedWriter writer = new BufferedWriter(fw); 
			for(Order o: soldOrders){
				sold.add(o.getId());
				writer.write(o.getId());
				writer.newLine();
				removeOrder(o.getId());
			}
			writer.close();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
